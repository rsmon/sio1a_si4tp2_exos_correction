package si4tp2_exos;

public class SI4TP2_Exo3b {

    public static void main(String[] args) {
        
        float[] notes = { 12, 8, 14, 7, 9.5f, 14, 15.5f, 6, 16, 11, 14, 10.5f };
 
        float total=0, max=0, min=20, moyenne;
         
        int   nbNotes=0;
        
        //Solution avec une boucle our chaque ( for each)
          
        for( float x : notes){
              
              total += x; nbNotes++;
              
              if( x < min )
              {
                
                  min = x;    
              }
              else
              {
                  if ( x > max ){
                  
                     max = x ;
                  } 
              }
        }
        
        moyenne=total/nbNotes;
        
        System.out.printf("\nMinimum=%2.2f\n",min);
        System.out.printf("\nMaximum=%2.2f\n",max);
        System.out.printf("\nMoyenne=%2.2f\n\n",moyenne);
   }        
}




