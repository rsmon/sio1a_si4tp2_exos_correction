package si4tp2_exos;

public class SI4TP2_Exo5 {

    public static void main(String[] args) {
        
         int[]   tableau = { 33, 56, 14, 76, 81, 2, 57, 13, 61, 64, 45, 35, 95, 15 };
        
         int nbPairs=0, totalPairs=0, nbImpairs=0,totalImpairs=0;
         
         float moyennePairs, moyenneImpairs;
         
         for (int x : tableau ) {
         
            if( x %2 == 0 )
            {
              totalPairs+=x;
              nbPairs++;
            }
            else
            {
              totalImpairs+=x;
              nbImpairs++;
            }
         }
            
         if ( nbPairs > 0 ){
         
             moyennePairs = (float) totalPairs/nbPairs;
             System.out.printf("\nMoyenne des nombres pairs = %2.2f\n",moyennePairs); 
         }
         else{
             System.out.println("\nIl n'y a pas de nombres pairs dans le tableau\n");
         }
         
         if ( nbImpairs > 0 ){
         
             moyenneImpairs= (float) totalImpairs/nbImpairs;
             System.out.printf("\nMoyenne des nombres impairs = %2.2f\n\n",moyenneImpairs); 
         }
         else{
             System.out.println("\nIl n'y a pas de nombres impairs dans le tableau\n\n");
         }    
    } 
}


