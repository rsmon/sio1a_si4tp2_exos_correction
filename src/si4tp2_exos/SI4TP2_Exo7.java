package si4tp2_exos;
import static java.lang.Math.*;
import java.util.Scanner;

public class SI4TP2_Exo7 {
   
    public static void main(String[] args) {
        
        Scanner  clavier=new Scanner(System.in); 
        
        String[] matiere  = { "acier", "aluminium", "plomb"};
        double[] densite  = { 7.5    , 2.7        ,  11.3  };
        
        String[] forme    = {"cube","boule","cylindre"};
        
        String[] message  = { "du cube ",
                              "de la boule ",
                              "du cylindre "
                            };
        
        double rayonBoule=0;
        double rayonCylindre=0;
        double arete=0;
        double hauteur=0;
        
        double volume=0;
        double poids=0;
        
        int choixForme=0;
       
        System.out.println("\nForme de l'objet ( Répondre par 0, 1 ou 2 )\n");
        System.out.println("Cube:     0");
        System.out.println("Boule:    1");
        System.out.println("Cylindre: 2");
        System.out.println();
        
        choixForme=clavier.nextInt();
      
        int choixMatiere=0;
        
        System.out.println("\nMatière dont est constitué l'objet( Répondre par 0, 1 ou 2 )\n");
        System.out.println("Acier:     0");
        System.out.println("Aluminium:    1");
        System.out.println("Plomb: 2");
        System.out.println();
        
        choixMatiere=clavier.nextInt();
      
        switch ( choixForme)
        {
        
            case 0: 
                
                  System.out.println("\nCombien mesure l'arête du cube en cm?\n");
                  arete=clavier.nextDouble();
                
                  volume= pow(arete,3);
        
                  break;
            
            case 1: 
                
                   System.out.println("\nCombien mesure le rayon de la boule en cm?\n");
                   rayonBoule=clavier.nextDouble();
                
                   volume= 4*PI*pow(rayonBoule,3)/3;
          
                   break;
                
             case 2: 
                
                   System.out.println("\nQuelle est la hauteur du cylindre en cm?\n");
                   hauteur=clavier.nextDouble();
                 
                   System.out.println("\nCombien mesure le rayon du cylindre en cm?\n");
                   rayonCylindre=clavier.nextDouble();
                
                   volume= PI*pow(rayonCylindre,2)*hauteur;
        
                   break;   
        }
       
        poids=volume*densite[choixMatiere];   
        
        double poidsAff=poids;
        String unites="gr";
        
        if (poids>1000){poidsAff=poids/1000;unites="kg";}
        
        System.out.printf("\nLe poids %s en %s est: %8.3f %s\n\n",
                          message[choixForme],matiere[choixMatiere],poidsAff,unites
                         );         
    } 
       
}


