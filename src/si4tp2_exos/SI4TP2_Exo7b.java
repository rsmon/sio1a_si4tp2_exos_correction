package si4tp2_exos;

public class SI4TP2_Exo7b {

    public static void main(String[] args) {
        
         float[]   tabNotes = { 12, 8, 14, 11, 10f, 14, 15.5f, 16, 16, 11, 14, 10.5f ,12, 8,12.5f};
         int       effectif=tabNotes.length;
         
         String[]  tabLibMentions={"Très Bien","Bien","Assez Bien","Passable","Refusé"};
         int       lgTabsMentions=tabLibMentions.length;
         
         int[]    tabEffMentions= new int[lgTabsMentions];
         
         for ( float note : tabNotes){
          
             if      (note>=16)  tabEffMentions[0]++;
             else if (note>=14)  tabEffMentions[1]++;
             else if (note>=12)  tabEffMentions[2]++;
             else if (note>=10)  tabEffMentions[3]++;
             else                tabEffMentions[4]++;
         }
         
         System.out.println("\nStatistique Examen\n");
         
         for(int i=0;i<lgTabsMentions;i++){
         
             float pctMention = (float) tabEffMentions[i]*100/effectif;
             
             System.out.printf("%-15s %6d %5.2f %%\n",tabLibMentions[i],tabEffMentions[i],pctMention);     
         }
         
         int   effRecus = effectif - tabEffMentions[4];
         float pctRecus = (float) effRecus * 100 / effectif;
        
         System.out.printf("\nPourcentage de reçus %8.2f %%\n\n",pctRecus);     
    }
}


