package si4tp2_exos;

public class SI4TP2_Exo3 {

    public static void main(String[] args) {
        
        float[] notes = { 12, 8, 14, 7, 9.5f, 14, 15.5f, 6, 16, 11, 14, 10.5f };
 
        float total=0, max=0, min=20, moyenne;
         
        int   nbNotes=0;
        
        //Solution avec une boucle indicée
          
        for( int indice=0; indice<notes.length ;indice++){
              
              total += notes[indice]; nbNotes++;
              
              if(notes[indice]<min )
              {
                
                  min=notes[indice];    
              }
              else
              {
                  if (notes[indice] > max){
                  
                     max=notes[indice];
                  } 
              }
        }
        
        moyenne=total/nbNotes;
        
        System.out.printf("\nMinimum=%2.2f\n",min);
        System.out.printf("\nMaximum=%2.2f\n",max);
        System.out.printf("\nMoyenne=%2.2f\n\n",moyenne);
   }        
}




