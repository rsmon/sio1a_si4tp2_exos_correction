package si4tp2_exos;

public class SI4TP2_Exo6 {

    public static void main(String[] args) {
        
         String[]   fruits = {"pomme",     "poire",     "abricot", "ananas",    "citron",
                              "pêche",     "mirabelle", "fraise",  "framboise", "raisin",
                              "groseille", "prune",     "orange",  "banane" };
     
         System.out.println();
         
         for(String fruit : fruits){
         
           if ( fruit.length()== 6  ){
           
               System.out.println(fruit);
           
           }
         }
    
         System.out.println();
    }
}
  
