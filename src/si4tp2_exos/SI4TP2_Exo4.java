package si4tp2_exos;

public class SI4TP2_Exo4 {

  public static void main(String[] args) {
        
     int[]   tableau = { 33, 56, 14, 76, 81, 2, 57, 13, 61, 64, 45, 35, 95, 15 };

     System.out.println("\nListe des entiers  du tableau impairs et supérieurs à 50.\n");
     
     for( int   x  : tableau ){
     
         if(  x % 2 == 1  &&  x>50  ){

             System.out.println(x);
             
         }
     } 
      System.out.println();
  } 
}

 

